import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class Main {

    private final static Set<String> urlList = new TreeSet<>();
    private static final Set<String> sortedUrlList = new TreeSet<>(Comparator.comparing(String::trim));

    public static synchronized boolean insertUrl(String url) {
        return urlList.add(url);
    }

    public static void main(String[] args) throws IOException{
        String testUrl = "https://lenta.ru/";
/*        insertUrl(testUrl);
        new AddRecursiveUrlList(testUrl).invoke();

        for (String url : urlList) {
            url = "\t".repeat(url.replaceAll("[^/]", "").length() - 3) + url; //тут, конечно, фигня, но я уже решил не допиливать, т.к. сделал еще решение через класс
            sortedUrlList.add(url);
        }

        FileWriter fileWriter = new FileWriter("c:/temp/res1.txt");
        for (String str : sortedUrlList) {
            fileWriter.write(str + "\r\n");
        }
        fileWriter.close();*/

        Link.saveToFile(new Link(new URL(testUrl), 0).invoke(), "c:/temp/res3.txt");
    }
}
