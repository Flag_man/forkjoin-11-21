import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

public class Link extends RecursiveTask<Link> implements Comparable<Link> {

    private final URL url;
    private int depth;
    private final Set<Link> childrenOfLink = new TreeSet<>();

    public Link(URL url, int depth) {
        this.url = url;
        this.depth = depth;
    }

    public synchronized Link addIfContainsLink(Link newLink) {
        for (Link link : childrenOfLink) {
            if (link.url.toString().equals(newLink.url.toString())) {
                return null;
            }
        }
        childrenOfLink.add(newLink);
        return newLink;
    }

    @Override
    protected Link compute() {
        try {
            Thread.sleep(150);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        List<Link> taskList = new ArrayList<>();
        String href = url.toString();
        depth++;

        try {
            Elements hrefList = Jsoup.connect(href).maxBodySize(0).get().select("a[href]").select("[href$=/]");
            for (Element tempHref : hrefList) {
                String tempHrefAbsUrl = tempHref.absUrl("href");
                URL tempUrl = new URL(tempHrefAbsUrl);

                if (!tempHrefAbsUrl.equals(href) && tempHrefAbsUrl.startsWith(href)) {
                    Link task = addIfContainsLink(new Link(tempUrl, depth));
                    if (task != null) {
                        taskList.add((Link) task.fork());
                    }
                }
            }
        } catch (MalformedURLException malformedURLException) {
            malformedURLException.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        taskList.forEach(ForkJoinTask::join);
        return this;
}

    @Override
    public int compareTo(Link o) {
        return url.toString().compareTo(o.url.toString());
    }

    public static void fillTreeSet(Link link, Set<LinkWithDepth> treeSet) {
        treeSet.add(new LinkWithDepth(link.url.toString(), link.depth));
        for (Link child : link.childrenOfLink) {
            fillTreeSet(child, treeSet);
        }
    }

    public static void saveToFile(Link link, String fileName) throws IOException {
        Set<LinkWithDepth> treeSetToSave = new TreeSet<>();
        fillTreeSet(link, treeSetToSave);
        FileWriter fileWriter = new FileWriter(fileName);
        for (LinkWithDepth linkWithDepth : treeSetToSave) {
            fileWriter.write("\t".repeat(linkWithDepth.getDepth() - 1) + linkWithDepth.getUrl() + "\r\n");
        }
        fileWriter.close();
    };
}
