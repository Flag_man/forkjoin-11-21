import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveAction;

public class AddRecursiveUrlList extends RecursiveAction {

    public String href;

    public AddRecursiveUrlList(String href) {
        this.href = href;
    }

    @Override
    protected void compute() {
        try {
            try {
                Thread.sleep(150);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            List<AddRecursiveUrlList> taskList = new ArrayList<>();
            Elements hrefList = Jsoup.connect(href).maxBodySize(0).get().select("a[href]").select("[href$=/]");

            for (Element tempHref : hrefList) {
                if (!tempHref.absUrl("href").equals(href) && tempHref.absUrl("href").startsWith(href) && Main.insertUrl(tempHref.absUrl("href")) ) {
                    AddRecursiveUrlList task = new AddRecursiveUrlList(tempHref.absUrl("href"));
                    taskList.add((AddRecursiveUrlList) task.fork());
                }
            }

            taskList.forEach(ForkJoinTask::join);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
