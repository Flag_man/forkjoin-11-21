public class LinkWithDepth implements Comparable<LinkWithDepth> {

    private String url;
    private int depth;

    public LinkWithDepth(String url, int depth) {
        this.url = url;
        this.depth = depth;
    }

    public String getUrl() {
        return url;
    }

    public int getDepth() {
        return depth;
    }

    @Override
    public int compareTo(LinkWithDepth o) {
        return getUrl().compareTo(o.getUrl());
    }
}
